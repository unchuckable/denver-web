package com.github.unchuckable.denver.util;

import java.nio.channels.CompletionHandler;
import java.util.function.BiConsumer;

public class Complete {

	public static <T,A> CompletionHandler<T,A> with( BiConsumer<T, A> successHandler, BiConsumer<Throwable, A> errorHandler ) {
		return new CompletionHandler<T, A>() {

			@Override
			public void completed(T result, A attachment) {
				successHandler.accept( result, attachment );
			}

			@Override
			public void failed(Throwable exc, A attachment) {
				errorHandler.accept(exc, attachment );
				
			}
		};
	}

}
