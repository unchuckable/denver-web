package com.github.unchuckable.denver.filters;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.github.unchuckable.denver.ansi.AnsiToHtmlConverterStream;

public class InlinePictureFilter implements AnsiToHtmlConverterStream.LineFilter{

	private static final Pattern picturePattern = Pattern.compile( "\\WIC Picture: (?:<[^>]+>)*([^<]+)");
	
	@Override
	public String filter(String line) {
		Matcher matcher = picturePattern.matcher( line );
		if ( matcher.find() ) {
			System.out.println( line );
			System.out.println( matcher.group( 1 ) );
			return "<img class=\"float\" src=\""+ matcher.group( 1 ) + "\"/>";
		}
		return line;
	}

	
	
}
