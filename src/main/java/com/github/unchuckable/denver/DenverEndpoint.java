package com.github.unchuckable.denver;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.AsynchronousSocketChannel;

import javax.websocket.OnClose;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.ServerEndpoint;

import com.github.unchuckable.denver.ansi.AnsiToHtmlConverterStream;
import com.github.unchuckable.denver.filters.InlinePictureFilter;
import com.github.unchuckable.denver.util.Complete;

@ServerEndpoint("/denver")
public class DenverEndpoint {

	public static class ConnectionContext {
		private final Session session;
		private final AsynchronousSocketChannel channel;
		private final ByteBuffer buffer;
		private final AnsiToHtmlConverterStream converterStream;
		private final StringBuilder messageBuilder;

		public ConnectionContext(Session session, AsynchronousSocketChannel channel) throws IOException {
			this.session = session;
			this.channel = channel;
			this.buffer = ByteBuffer.wrap(new byte[8192]);
			this.messageBuilder = new StringBuilder(1024);
			this.converterStream = new AnsiToHtmlConverterStream(messageBuilder::append)
					.withFilters( new InlinePictureFilter() );
		}

		public Session getSession() {
			return session;
		}

		public AsynchronousSocketChannel getChannel() {
			return channel;
		}

		public ByteBuffer getBuffer() {
			return buffer;
		}

		public AnsiToHtmlConverterStream getConverterStream() {
			return converterStream;
		}

		public String getOutput() {
			String result = messageBuilder.toString();
			messageBuilder.setLength(0);
			return result;
		}
	}

	@OnOpen
	public void onOpen(Session session) throws IOException {
		AsynchronousSocketChannel channel = AsynchronousSocketChannel.open();
		ConnectionContext ctx = new ConnectionContext(session, channel);

		session.getUserProperties().put("ctx", ctx);

		channel.connect(new InetSocketAddress("aelfhame.net", 1999), ctx, Complete.with(this::connected, this::error));
	}

	private void connected(Object input, ConnectionContext ctx) {
		ctx.getSession().getAsyncRemote().sendText("Connected to server...");
		ctx.getChannel().read(ctx.getBuffer(), ctx, Complete.with(this::received, this::error));
	}

	private void received(Integer bytes, ConnectionContext ctx) {
		if (bytes == -1) {
			try {
				ctx.getSession().close();
				ctx.getChannel().close();
			} catch (IOException e) {
				e.printStackTrace();
			}
			return;
		}

		if (ctx.getSession().isOpen()) {
			ctx.getBuffer().flip();
			ctx.getConverterStream().evaluate( ctx.getBuffer() );
			ctx.getSession().getAsyncRemote().sendText(ctx.getOutput());
		}

		ctx.getBuffer().clear();
		ctx.getChannel().read(ctx.getBuffer(), ctx, Complete.with(this::received, this::error));
	}

	private void error(Throwable error, Object attachment) {
		error.printStackTrace();
	}

	@OnClose
	public void onClose(Session session) {
		try {
			getConnectionContext(session).getChannel().shutdownInput();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@OnMessage
	public void onMessage(Session session, String message) {
		getConnectionContext(session).getChannel().write(ByteBuffer.wrap((message).getBytes()));
	}

	private ConnectionContext getConnectionContext(Session session) {
		return (ConnectionContext) session.getUserProperties().get("ctx");
	}

}
