package com.github.unchuckable.denver.ansi;

import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Consumer;

public class AnsiToHtmlConverterStream {

	@FunctionalInterface
	public interface LineFilter {
		public String filter( String line );
	}
	
	private static final String[] ANSI_COLORS = new String[] {
			"gray", "red", "green", "yellow", "blue", "magenta", "cyan", "white"
	};

	private enum Handler {
		TEXT {
			@Override
			public Handler handleByte(byte b, AnsiToHtmlConverterStream parent)  {
				switch ( b ) {
				case '\r' : return this;
				case '\n' : parent.sendLine();
							return NON_TEXT;
				case '\t' : parent.addToLine( "&nbsp;&nbsp;&nbsp;&nbsp;");
				            parent.addCharacters( 4 );
							return this;
				case '<' :  parent.addToLine( "&lt;" );
				            parent.addCharacter();
							return this;
				case ' ' :  return TEXT_SPACE;
				case 27  :  return ESCAPED.handleByte( b, parent );
				default  :  parent.addToLine( b );
				            parent.addCharacter();
							return this;
				}
			}			
		},
		NON_TEXT {
			@Override
			public Handler handleByte(byte b, AnsiToHtmlConverterStream parent) {
				if ( b == ' ' ) {
					return SPACES;
				}
				else {
					return TEXT.handleByte(b, parent);
				}
			}
		},
		TEXT_SPACE {
			@Override
			public Handler handleByte(byte b, AnsiToHtmlConverterStream parent) {
				if ( b == ' ' ) {
					return SPACES.handleByte(b, parent);
				}
				else {
					parent.addToLine( " " );
					return TEXT.handleByte(b, parent);
				}
			}
			
		},
		SPACES {
			@Override
			public Handler handleByte(byte b, AnsiToHtmlConverterStream parent) {
				if ( b == ' ' ) {
					parent.spaces ++;
					return this;
				}
				else {
					for ( int i = 0; i < parent.spaces; i++ ) {
						parent.addToLine( "&nbsp;" );
					}
					parent.addCharacters( parent.spaces );
					parent.spaces = 1;
					return TEXT.handleByte(b, parent);
				}
			}
		},
		ESCAPED {

			@Override
			public Handler handleByte(byte b, AnsiToHtmlConverterStream parent) {
				switch ( b ) {
				case 27: return this;
				case '[': return this;
				case 'm' : evaluateEscapeSequence( parent );
						   return NON_TEXT;
				default:   appendToEscapeSequence( b, parent );
						   return this;
				}
			}

			private void appendToEscapeSequence(int b, AnsiToHtmlConverterStream parent) {
				parent.ansiString.append( (char) b );
			}

			
			private void evaluateEscapeSequence(AnsiToHtmlConverterStream parent) {
				String fullAnsiCommand = parent.ansiString.toString();
				parent.ansiString.setLength(0);
				
				String[] parts = fullAnsiCommand.split(";");

				for ( String thisPart : parts ) {
					int value = Integer.parseInt( thisPart );
					if ( value == 0 ) {
						parent.setForeground( "" );
					}
					if ( value >= 30 && value <= 38 ) {
						parent.setForeground( ANSI_COLORS[ value - 30 ] );
					}
				}
			}
			

		};
		
		public abstract Handler handleByte( byte b, AnsiToHtmlConverterStream parent );
	}
	
	private final Consumer<String> output;
	private final StringBuilder lineBuilder;
	private int spaces = 0;
	private Handler currentHandler;
	private int length = 0;
	
	private List<LineFilter> lineFilters = new ArrayList<>();
	
	private StringBuilder ansiString = new StringBuilder();
	private String foreground = "";
	
	private String lastForeground = "";
	
	public AnsiToHtmlConverterStream( Consumer<String> output ) {
		this.output = output;
		this.currentHandler = Handler.NON_TEXT;
		this.lineBuilder = new StringBuilder( 1024 );
	}
	
	public AnsiToHtmlConverterStream withFilters( LineFilter ... filters ) {
		lineFilters.addAll( Arrays.asList( filters ) );
		return this;
	}
	
	public void evaluate( ByteBuffer buffer ) {
		while ( buffer.hasRemaining() ) {
			currentHandler = currentHandler.handleByte( buffer.get(), this );
		}
	}
	
	public void setForeground( String foreground ) {
	  this.foreground = foreground;
	}

	private String styleString( String foreground ) {
	  return foreground.isEmpty() ? "" : "color:" + foreground;
	}
	
	private void updateAnsi() {
	  if (! foreground.equals( lastForeground ) ) {
	    if ( lastForeground != null ) {
	      lineBuilder.append( "</span>" );
	    }
	    lineBuilder.append("<span style=\"" + styleString( foreground ) + "\">");
	    lastForeground = foreground;
	  }
	}
	
	private void addToLine( String string ) {
	  updateAnsi();
	  lineBuilder.append( string );
	}
	
	private void addToLine( int ch ) {
	  updateAnsi();
	  lineBuilder.append( (char) ch );
	}
	
	private void addCharacter() {
	  length ++;
	}
	
	private void addCharacters( int count ) {
	  length += count;
	}
	
	private void resetLength() {
	  this.length = 0;
	}
	
	private void sendLine() {
	    if (! foreground.isEmpty() ) {
	      lineBuilder.append( "</span>" );
	    }
	  
		String line = lineBuilder.toString();
		lineBuilder.setLength(0);
		
		for ( LineFilter thisFilter : lineFilters ) {
			line = thisFilter.filter( line );
		}			
	
		String lineClass = length > 160 ? "line long" : "line";
		resetLength();
		
	    output.accept( "<div class=\"" + lineClass + "\">" + line + "</div>\n" );
	    lastForeground = null;
	}
	
}
