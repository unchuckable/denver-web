var conn;

function getWebSocketAddress( subpath ) {
	var loc = window.location, new_uri;
	if (loc.protocol === "https:") {
	    new_uri = "wss:";
	} else {
	    new_uri = "ws:";
	}
	new_uri += "//" + loc.host;
	new_uri += loc.pathname + subpath;
	return new_uri;
}

function connect() {
    conn = new WebSocket( getWebSocketAddress( "denver" ) );
    conn.onmessage = (msg) => {
    	cOut = document.getElementById( "output" );
        let cValue = cOut.innerHTML;
        cOut.innerHTML = cValue + msg.data + "\n";
        cOut.scrollTop = cOut.scrollHeight;
    }
    
    conn.onclose = function( e ) {
    	document.getElementById("disconnect").setAttribute( "disabled", "disabled" );
    	document.getElementById("connect").removeAttribute( "disabled" );
    }
    
    document.getElementById("connect").setAttribute("disabled", "disabled");
    document.getElementById("disconnect").removeAttribute( "disabled" );
    
    document.getElementById( "input" ).onkeypress = function( key ) {
    	if ( key.which == 13 ) {
    		sendMessage();
    		key.preventDefault();
    	}
    };
}

function disconnect() {
    conn.close();
    document.getElementById("input").innerHTML = "";
    document.getElementById("disconnect").setAttribute("disabled", "disabled");
    document.getElementById("connect").removeAttribute("disabled");
}

function sendMessage() {
	var input = document.getElementById("input");
    conn.send(input.value + "\n" );
    input.value = "";
}

